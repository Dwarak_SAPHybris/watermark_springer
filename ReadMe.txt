I have implemented this watermark service using below technology stack

1.Java
2.Spring
3.Tomcat

Steps to test the service.

-Deploy the war in tomcat and you can use below rest calls to test the multiple APIs.

"/addDocument" rest call accepts document information and creates document id.

1. http://localhost:8087/watermark/addDocument      	POST

payload: {
		"content":"book",
		"topic":"The Power Of Habit",
		"author":"Charles Duhigg",
		"title":"business"
		}
	

"/addWaterMarks" rest call creates the watermark and returns the ticket	
2. http://localhost:8087/watermark/addWaterMarks    	POST

payload: {
		"documentId":5
		}
	
"/document" rest call returns the document details	
3. http://localhost:8087/watermark/document 			POST

payload: {
		"documentId":5,
		"ticketId":"b42e5e73-10bc-4cc5-847e-f376b8477917"//this is just a sample ticket id
		}
	
