package com.springer.document.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import com.springer.document.entity.Book;
import com.springer.document.entity.BookTopic;
import com.springer.document.entity.Document;
import com.springer.document.entity.Journal;

/**
 * Simple, not thread-safe, in-memory Document store.
 */
@Repository
public class DocumentRepository {

	private final Map<Integer, Document> documents = new ConcurrentHashMap<Integer, Document>();

	@PostConstruct
	public void initDemoDocuments() {
		saveDocument(new Book(1, "A Brief History of Time", "Stephen Hawking", BookTopic.SCIENCE));
		saveDocument(new Book(2, "Monk Who Sold His Ferrari", "Robin Sharma", BookTopic.BUSINESS));
		saveDocument(new Journal(3, "Journal of human flight routes", "Clark Kent"));
	}

	public Optional<Document> getDocument(int documentId) {
		System.out.println("in repo:"+documentId);
		System.out.println(documents.get(documentId) == null);
		return Optional.ofNullable(documents.get(documentId));
	}

	public void saveDocument(Document document) {
		documents.put(document.getId(), document);
	}
	
	public int getDocumentsSize(){
		return documents.size();
	}
}
