package com.springer.document.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Repository;

import com.springer.document.entity.WatermarkJob;

/**
 * Simple, not thread-safe, in-memory WatermarkJob store.
 */
@Repository
public class WatermarkJobRepository {

	private final Map<UUID, WatermarkJob> jobs = new ConcurrentHashMap<UUID, WatermarkJob>();

	/**
	 * Creates a job in state {@link com.springer.document.entity.WatermarkJob.State#PROCESSING} for given document id.
	 */
	public WatermarkJob createJob(int documentId) {
		WatermarkJob job = new WatermarkJob(documentId);
		jobs.put(job.getTicketId(), job);
		return job;
	}

	/**
	 * Loads the Job from the story. Returns empty result if not found.
	 */
	public Optional<WatermarkJob> getJob(UUID ticket) {
		System.out.println("jobs size::"+jobs.size());
		return Optional.ofNullable(jobs.get(ticket));
	}

	/**
	 * Loads the Job from the story. Takes a filter parameter that constrains the query to the given documentId.
	 * Returns empty result if not found.
	 */
	public Optional<WatermarkJob> getJob(UUID ticket, int documentId) {
		System.out.println("jobs size222 sizee::"+jobs.size());
		System.out.println("all jobs::"+jobs.toString());
		System.out.println("ticket::"+ticket);
		System.out.println("ui document id::"+documentId);
		//System.out.println("documentId;;form repo::"+jobs.get(ticket).getDocumentId());
		return getJob(ticket).filter(j -> j.getDocumentId() == documentId);
	}
}
