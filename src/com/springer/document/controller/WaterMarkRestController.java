/**
 * 
 */
package com.springer.document.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springer.document.entity.Book;
import com.springer.document.entity.BookTopic;
import com.springer.document.entity.Document;
import com.springer.document.entity.Journal;
import com.springer.document.service.DocumentService;
import com.springer.document.service.WatermarkJobService;

@RestController
@RequestMapping
public class WaterMarkRestController {
	
	@Autowired
	private WatermarkJobService watermarkJobService;
	
	@Autowired
	private DocumentService documentService;
	
	/**
	 * Get details for ticket id.
	 * @param document
	 * @return
	 */
	@RequestMapping(value="/document", method = RequestMethod.POST)
	public Optional<Document> getDocumentDetailsForTicket(@RequestBody Map<String, String> document){
		UUID ticketId = UUID.fromString(document.get("ticketId"));
		int documentId = Integer.parseInt(document.get("documentId"));
		System.out.println("ticket:::"+ticketId);
		System.out.println("documentId:::"+documentId);
		return documentService.getDocument(ticketId, documentId);
	}
	
	/**
	 * Provide the details of document to add to repository
	 * @param document
	 * @return
	 */
	@RequestMapping(value="/addDocument", method=RequestMethod.POST)
	public Map<String, String> addDocument(@RequestBody Map<String, String> document){
		int totalDocuments = documentService.getDocumentSize();
		Document doc = null;
		if(document.get("content").equals("book")){
			String topic = document.get("topic");
			BookTopic bookTopic = null;
			if(topic.equals("business"))
					bookTopic = BookTopic.BUSINESS;
			else if(topic.equals("science"))
				bookTopic = BookTopic.SCIENCE;
			else if(topic.equals("business"))
				bookTopic = BookTopic.BUSINESS;
			doc = new Book(totalDocuments+1, document.get("title"), document.get("author"), bookTopic);
		}else if(document.get("content").equals("journal")){
			doc = new Journal(totalDocuments+1, document.get("author"), document.get("title"));
		}
		documentService.addDocument(doc);
		return pairOf("documentId", totalDocuments+1);
	}
	
	
	/**
	 * Provide document id and ticket id and service creates the watermark
	 * @param document
	 * @return
	 */
	@RequestMapping(value="/addWaterMarks", method=RequestMethod.POST)
	public Map<String, String> addWaterMarksToDocuments(@RequestBody Map<String, Integer> document){
		
		System.out.println(document);
		
		return documentService.getDocument(document.get("documentId"))
							.map(watermarkJobService::createJob)
							.map(ticket -> pairOf("ticketId", ticket)).get();
	}
	
	@SuppressWarnings("serial")
	private HashMap<String, String> pairOf(String key, Object value) {
		return new HashMap<String, String>() {
			{
				put(key, value.toString());
			}
		};
	}

}
