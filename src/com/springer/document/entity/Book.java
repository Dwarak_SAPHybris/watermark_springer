package com.springer.document.entity;

import static com.springer.document.entity.Document.DocumentType.BOOK;

/**
 * Concrete document implementation for {@link com.springer.document.entity.Document.DocumentType#BOOK}.
 */
public class Book extends Document {

	
	private final BookTopic topic;

	public Book(int id, String title, String author, BookTopic topic) {
		super(id, title, author);
		this.topic = topic;
	}

	@Override
	public DocumentType getDocumentType() {
		return BOOK;
	}

	public BookTopic getTopic() {
		return topic;
	}
}
