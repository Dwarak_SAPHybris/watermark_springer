package com.springer.document.entity;

/**
 * Topic of documents. Usually only supported by books.
 */
public enum BookTopic {

	BUSINESS,

	SCIENCE,

	MEDIA
}
