package com.springer.document.entity;

import java.util.Optional;

/**
 * A document is a publication which can be identified via author, title and content type.
 * After the watermarking processing is finished for a document the watermark property should be set.
 */
public abstract class Document {

	public static enum DocumentType {

		BOOK, JOURNAL
	}

	private final int id;

	private final String title;
	
	private final String author;

	private Optional<Watermark> watermark;

	/**
	 * @param id   Unique id of the document
	 * @param author  author for the document 
	 * @param title  title of the document
	 */
	public Document(int id, String title, String author) {
		this.id = id;
		this.title = title;
		this.author = author;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}


	public abstract DocumentType getDocumentType();

	public Optional<Watermark> getWatermark() {
		return watermark;
	}

	public void setWatermark(Watermark watermark) {
		this.watermark = Optional.ofNullable(watermark);
	}

}
