package com.springer.document.entity;

import static com.springer.document.entity.Document.DocumentType.JOURNAL;

/**
 * Concrete document implementation for {@link com.springer.document.entity.Document.DocumentType#JOURNAL}.
 */
public class Journal extends Document {

	public Journal(int id, String author, String title) {
		super(id, author, title);
	}

	@Override
	public DocumentType getDocumentType() {
		return JOURNAL;
	}
}
