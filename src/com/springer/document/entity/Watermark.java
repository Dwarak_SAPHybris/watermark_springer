package com.springer.document.entity;

import java.util.Optional;

/**
 * Represents properties of a watermark.
 */
public class Watermark {

	public static class Builder {
		private String author;
		private String title;
		private Document.DocumentType documentType;
		private Optional<BookTopic> topic = Optional.empty();

		public Builder setAuthor(String author) {
			this.author = author;
			return this;
		}

		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder setDocumentType(Document.DocumentType documentType) {
			this.documentType = documentType;
			return this;
		}

		public Builder setTopic(BookTopic topic) {
			this.topic = Optional.ofNullable(topic);
			return this;
		}

		public Watermark build() {
			return new Watermark(author, title, documentType, topic);
		}
	}

	private final String author;

	private final String title;

	private final Document.DocumentType documentType;

	private final Optional<BookTopic> topic;

	private Watermark(String author, String title, Document.DocumentType documentType, Optional<BookTopic> topic) {
		this.author = author;
		this.title = title;
		this.documentType = documentType;
		this.topic = topic;
	}

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public Document.DocumentType getDocumentType() {
		return documentType;
	}

	public Optional<BookTopic> getTopic() {
		return topic;
	}
}
