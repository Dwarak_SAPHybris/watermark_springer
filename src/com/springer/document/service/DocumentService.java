package com.springer.document.service;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springer.document.Repository.DocumentRepository;
import com.springer.document.entity.Document;

/**
 * Accesses documents and it's related business logic.
 */
@Service
@Configurable
public class DocumentService {

	@Autowired
	private WatermarkJobService watermarkJobService;

	@Autowired
	private DocumentRepository repository;

	/**
	 * Returns the document for the given id. If the given ticket is invalid, will return an empty result.
	 *
	 * @return Returns the document for the given id.
	 */
	public Optional<Document> getDocument(UUID ticket, int documentId) {
		if (watermarkJobService.isValidTicket(ticket, documentId)) {
			return getDocument(documentId);
		}
		return Optional.empty();
	}

	/**
	 * Returns the document for the given id. It possibly has no watermark yet.
	 * 
	 * @return Returns the document for the given id.
	 */
	public Optional<Document> getDocument(int documentId) {
		return repository.getDocument(documentId);
	}

	/**
	 * Saves the document as is updating the previous state.
	 */
	public void updateDocument(Document document) {
		repository.saveDocument(document);
	}
	
	public void addDocument(Document document) {
		repository.saveDocument(document);
	}
	
	public int getDocumentSize(){
		return repository.getDocumentsSize();
	}

	/**
	 * @return the watermarkJobService
	 */
	public WatermarkJobService getWatermarkJobService() {
		return watermarkJobService;
	}

	/**
	 * @param watermarkJobService the watermarkJobService to set
	 */
	public void setWatermarkJobService(WatermarkJobService watermarkJobService) {
		this.watermarkJobService = watermarkJobService;
	}

	/**
	 * @return the repository
	 */
	public DocumentRepository getRepository() {
		return repository;
	}

	/**
	 * @param repository the repository to set
	 */
	public void setRepository(DocumentRepository repository) {
		this.repository = repository;
	}

}
